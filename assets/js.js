AOS.init();

document.addEventListener("DOMContentLoaded", function () {
    var arrow = document.querySelector('.arrow');

    // Initial check to see if the arrow should be hidden
    checkScroll();

    // Listen for scroll events
    window.addEventListener('scroll', checkScroll);

    function checkScroll() {
        // If the user has scrolled down, hide the arrow; otherwise, show it
        if (window.scrollY < 700) {
            arrow.style.opacity = 0;
        } else {
            arrow.style.opacity = 1;
        }
    }
});